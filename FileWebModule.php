<?php

namespace thyseus\files;

use Yii;
use yii\i18n\PhpMessageSource;

/**
 * File module definition class
 */
class FileWebModule extends \yii\base\Module
{
    public $version = '0.5.0-dev';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'thyseus\files\controllers';

    public $defaultRoute = 'files\files\index';

    /**
     * @var string The class of the User Model inside the application this module is attached to
     */
    public $userModelClass = 'app\models\User';

    /**
     * @var string Url to upload files.
     */
    public $uploadUrl = ['/files/file/upload'];

    /**
     * @var string the directory on the hard drive where the _protected_ upload files should be saved.
     * Please ensure sure the folder exists.
     * The default is Yii::$app->basePath . 'runtime/uploads/<user_id>/'
     * 0 is being inserted as user_id if a upload occured as guest
     *
     * If you want ALL files to be public accessible, you can change the path here to a public accessible
     * path, but use this with caution ! <!!danger!!>
     */
    public $uploadPath = null;

    /**
     * @var null same as above, but for public files. This files are saved in a public available folder so
     * we avoid to make a extra php request for public files.
     *
     * The default is Yii::$app->basePath . 'web/uploads/<user_id>/'
     * 0 is being inserted as user_id if a upload occured as guest
     */
    public $uploadPathPublic = null;

    /**
     * @var string Callback that defines which users choosable to share files with.
     *
     * For example, to allow only username foo and bar, do this:
     *
     * 'shareableUsersCallback' => function ($users) {
     *    return array_filter($users, function ($user) {
     *      return !in_array($user->username, ['foo', 'bar']); // or !$user->isAdmin()
     *    });
     *  },
     *
     */
    public $shareableUsersCallback = null;

    /**
     * @var bool|callable
     * Set to true to globally allow the deletion of files by the user who owns the file.
     * Set to false to globally disallow file deletion. Files can only be uploaded and never be removed.
     * Set a callback function to check if this specific file is allowed to be deleted.
     *
     * Note that files first go into a trash bin and can be restored. To finally delete them, the
     * user has to empty his trash bin manually.
     *
     * For example, to disallow the deletion of a specific tag:
     *
     * 'allowDeletion' => function($model) { return !in_array('not-deleteable', $model->tags); }
     *
     * You can specify a custom message why the file is not deleteable. Its only allowed of the return value is === true:
     *
     * 'allowDeletion' => function($model) { return time() > 618019215 ? 'File can not be deleted, Drachenlord is born' : true; }
     */
    public $allowDeletion = true;

    /**
     * @var array fill this array to let users tag their file with some of these options. For example:
     *
     * 'possibleTags' => [
     *      'logo' => 'Logo',
     *      'screenshot' => 'Screenshot',
     * ],
     * The key is stored in the database, the value is translated via Yii::t('app', as caption for the Tag.
     */
    public $possibleTags = [];

    /**
     * @var bool Set to true to skip the checksum integrity check for downloading files. Please note that this
     * does severly impact the security because files can be changed after uploaded and can get malicious content.
     */
    public $skipChecksumIntegrity = false;

    /** @var array The rules to be used in URL management. */
    public $urlRules = [
        'files/trash-bin' => 'files/file/trash-bin',
        'files/restore/<id>' => 'files/file/restore',
        'files/update/<id>' => 'files/file/update',
        'files/delete/<id>' => 'files/file/delete',
        'files/<id>' => 'files/file/view',
        'files/index' => 'files/file/index',
        'files/create' => 'files/file/create',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->uploadPath) {
            $this->uploadPath = Yii::$app->basePath . '/runtime/uploads';
        }

        if (!$this->uploadPathPublic) {
            $this->uploadPathPublic = Yii::$app->basePath . '/web/uploads';
        }

        if (!isset(Yii::$app->get('i18n')->translations['files*'])) {
            Yii::$app->get('i18n')->translations['files*'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'sourceLanguage' => 'en-US'
            ];
        }

        parent::init();
    }
}
