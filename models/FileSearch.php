<?php

namespace thyseus\files\models;

use thyseus\files\models\File;
use Yii;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * FilsSearch represents the model behind the search form about `app\models\File`.
 */
class FileSearch extends File
{
    public $trash = false;

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = File::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'public' => $this->public,
            'position' => $this->position,
            'status' => $this->status,
            'target_id' => $this->target_id,
        ]);

        if ($this->trash) {
            $query->andFilterWhere(['status' => File::STATUS_TRASHED]);
        } else {
            $query->andWhere(['!=', 'status', File::STATUS_TRASHED]);
        }

        $query->andWhere(['!=', 'status', File::STATUS_DELETED]);

        if ($this->created_by == -2) { # files shared with me
            $query->andFilterWhere([
                'like', 'shared_with', ', ' . Yii::$app->user->identity->username
            ]);
        } else {
            $query->andFilterWhere([
                'created_by' => Yii::$app->user->id,
            ]);
        }

        $query->andFilterWhere(['like', 'filename_user', $this->filename_user]);
        $query->andFilterWhere(['like', 'filename_path', $this->filename_path]);
        $query->andFilterWhere(['like', 'mimetype', $this->mimetype]);
        $query->andFilterWhere(['like', 'tags', $this->tags]);

        return $dataProvider;
    }
}
