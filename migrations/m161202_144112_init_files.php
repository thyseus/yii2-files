<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * @author Herbert Maschke <thyseus@gmail.com>
 */
class m161202_144112_init_files extends Migration
{
    public function up()
    {
        $tableOptions = '';

        if (Yii::$app->db->driverName == 'mysql')
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%files}}', [
            'id' => $this->primaryKey(),

            // dont use integer for target_id - it could be a slug, not only an numeric id
            'target_id' => $this->string()->null(),

            'target_url' => $this->string()->null(),
            'model' => $this->string()->null(),
            'title' => $this->string()->null(),
            'description' => $this->text()->null(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'filename_user' => $this->string()->notNull(),
            'filename_path' => $this->string()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(0),
            'mimetype' => $this->string()->null(),
            'public' => $this->integer()->notNull(),
            'position' => $this->integer()->notNull(),
            'download_count' => $this->integer()->notNull()->defaultValue(0),
            'tags' => $this->string()->null(),
            'shared_with' => $this->text()->null(),
            'slug' => $this->string()->notNull(),
            'checksum' => $this->string(32)->notNull(),
            'downloaded_from' => $this->string(255)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex(
            'files-slug',
            '{{%files}}',
            ['slug']
        );

        $this->createIndex(
            'files-target_id',
            '{{%files}}',
            ['target_id']
        );

        $this->createIndex(
            'files-tags',
            '{{%files}}',
            ['tags']
        );
    }

    public function down()
    {
        $this->dropTable('{{%files}}');
    }
}
